//
//  GIGPUPrograms.metal
//  Chapter3
//
//  Created by Gerard Iglesias on 07/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct Vertex {
  float4 position [[position]];
  float4 color; };

vertex Vertex vertex_main(device Vertex *vertices [[buffer(0)]], uint vid [[vertex_id]])
{
  Vertex v = vertices[vid];
  v.position +=  float4(0,0,0,0);
  return v;
}

fragment float4 fragment_main(Vertex inVertex [[stage_in]])
{
  return inVertex.color;
}
