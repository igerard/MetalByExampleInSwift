//
//  GIMTKView.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 03/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit
import Metal
import MetalKit
import simd


/// The Vertex structure
struct SimpleVertex {
  var position : float4
  var color : float4
}


class GIMTKView : MTKView {
  
  // MARK: Vars
  
  override var acceptsFirstResponder: Bool {
    get {
      return true
    }
  }
  
  override var canBecomeKeyView: Bool {
    get {
      return true
    }
  }
  
  var mlLayer : CAMetalLayer {
    get {
      return self.layer as! CAMetalLayer
    }
  }
  
  // MARK Data structure
  
  var vertexBuffer : MTLBuffer?
  
  var library : MTLLibrary?
  var vertexFunction : MTLFunction?
  var fragmentFunction : MTLFunction?
  
  var pipelineState : MTLRenderPipelineState?
  
  // MARK: Init Phase
  
  required init(coder : NSCoder){    
    super.init(coder : coder)
    
    acceptsTouchEvents = true
    preferredFramesPerSecond = 60
    
    let value = 0.85
    clearColor = MTLClearColor(red: value, green: value, blue: value, alpha: 0.5)
    clearDepth = 1.0
    clearStencil = 0
    colorPixelFormat = .bgra8Unorm
    sampleCount = 4
    
    device = MTLCreateSystemDefaultDevice()
    
    makeBuffers()
    makePipeline()
  }
  
  func makeBuffers(){
    let vertices : [SimpleVertex] = [
      SimpleVertex(position: float4(0, 0.5, 0, 1), color: float4(1, 0, 0, 1)),
      SimpleVertex(position: float4(-0.5, -0.5, 0, 1), color: float4(0, 1, 0, 1)),
      SimpleVertex(position: float4(0.5, -0.5, 0, 1), color: float4(0, 0, 1, 1))
    ]
    vertexBuffer = self.device?.makeBuffer(bytes: vertices, length: 3*MemoryLayout<SimpleVertex>.stride, options: .storageModeShared)
  }
  
  func makePipeline(){
    library = self.device?.makeDefaultLibrary()
    vertexFunction = library?.makeFunction(name : "vertex_main")
    fragmentFunction = library?.makeFunction(name : "fragment_main")
    
    let pipelineDescr = MTLRenderPipelineDescriptor()
    pipelineDescr.vertexFunction = vertexFunction
    pipelineDescr.fragmentFunction = fragmentFunction
    pipelineDescr.colorAttachments[0].pixelFormat = mlLayer.pixelFormat
    pipelineDescr.sampleCount = 4
    
    do {
      pipelineState = try device!.makeRenderPipelineState(descriptor: pipelineDescr)
    }
    catch {
      Swift.print(error)
    }
  }
  
  // MARK: Display
  
  override func draw(_ rect : CGRect) {
    guard self.device != nil else{
      Swift.print("No device")
      return
    }
    guard self.pipelineState != nil else{
      Swift.print("No pipeline")
      return
    }
    
    if let drawable = self.currentDrawable,
      let commandQueue = self.device?.makeCommandQueue(),
      let commandBuffer = commandQueue.makeCommandBuffer(),
      let passDescr = self.currentRenderPassDescriptor,
      let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){
      
      commandEncoder.setRenderPipelineState(pipelineState!)
      commandEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
      commandEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3)
      commandEncoder.endEncoding()
      commandBuffer.present(drawable)
      commandBuffer.commit()
    }
  }
  
  // MARK: Events
  
  override func becomeFirstResponder() -> Bool {
    return true
  }
  
  // MARK: Layouts
  
  // MARK: Archive
  
  override func awakeFromNib() {
    Swift.print(self.layer?.description ?? "No layer")
  }
}

