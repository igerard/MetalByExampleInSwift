import Foundation
import Metal
import simd

class GIMTKCamera
{
  var matrix : matrix_float4x4 = matrix_identity_float4x4
  
  var position : vector_float3 {
    didSet {
      _needsMatrixUpdate = true
    }
  }
  
  fileprivate var _needsMatrixUpdate : Bool = true
  fileprivate var _direction : vector_float3
  var direction : vector_float3 {
    set {
      _direction = normalize(newValue);
      _needsMatrixUpdate = true
    }
    get {
      return _direction
    }
  }
  
  fileprivate var _up : vector_float3
  var up : vector_float3
    {
    set {
      _up = normalize(newValue);
      _needsMatrixUpdate = true }
    get {
      return _up }
  }
  
  init()
  {
    position = float3(0.0)
    _direction = float3(0.0)
    _up = float3(0.0)
  }

  func GetViewMatrix() -> matrix_float4x4
  {
    if(_needsMatrixUpdate)
    {
      // compute eye referentiel
      let kp = _direction
      let ip = cross(kp, _up)
      let jp = cross(ip, kp)

      matrix.columns.0.x = ip.x
      matrix.columns.0.y = jp.x
      matrix.columns.0.z = kp.x
      matrix.columns.0.w = 0

      matrix.columns.1.x = ip.y
      matrix.columns.1.y = jp.y
      matrix.columns.1.z = kp.y
      matrix.columns.1.w = 0

      matrix.columns.2.x = ip.z
      matrix.columns.2.y = jp.z
      matrix.columns.2.z = kp.z
      matrix.columns.2.w = 0

      matrix.columns.3.x = -dot(ip, position)
      matrix.columns.3.y = -dot(jp, position)
      matrix.columns.3.z = -dot(kp, position)
      matrix.columns.3.w = 1

      _needsMatrixUpdate = false
    }
    return matrix
  }
};

func getPerpectiveProjectionMatrix(_ FieldOfView : Float, aspectRatio : Float, zFar : Float, zNear : Float) -> matrix_float4x4
{
  var m : matrix_float4x4 = matrix_float4x4()
  
  let f : Float = 1.0 / tan(FieldOfView / 2.0)
  
  m.columns.0.x = f / aspectRatio
  m.columns.1.y = f
  
  m.columns.2.z = zFar / (zFar - zNear)
  m.columns.2.w = 1.0
  
  m.columns.3.z = -(zNear*zFar)/(zFar-zNear)
  
  return m
}

func getLHOrthoMatrix(_ width : Float, height : Float, zFar : Float, zNear : Float) -> matrix_float4x4
{
  var m = matrix_float4x4()
  
  m.columns.0.x = 2.0 / width
  
  m.columns.1.y = 2.0 / height
  
  m.columns.2.z = 1.0 / (zFar-zNear)
  
  m.columns.3.z = -zNear / (zFar-zNear)
  m.columns.3.w = 1.0
  
  return m
}

func createPlane(_ device : MTLDevice) -> (MTLBuffer?, Int)
{
  var verts : [CFloat] = [ -1000.5, 0.0,  1000.5, 1.0,
                           1000.5, 0.0,  1000.5, 1.0,
                           -1000.5, 0.0, -1000.5, 1.0,
                           1000.5, 0.0,  1000.5, 1.0,
                           1000.5, 0.0, -1000.5, 1.0,
                           -1000.5, 0.0, -1000.5, 1.0,]
  let geoBuffer = device.makeBuffer(length: verts.count*MemoryLayout<CFloat>.size, options: MTLResourceOptions.storageModeManaged)
  if let geoPtr = geoBuffer?.contents().assumingMemoryBound(to: CFloat.self){
    geoPtr.assign(from: &verts, count: verts.count)
    geoBuffer!.didModifyRange(0..<verts.count*MemoryLayout<Float>.size)
  }
  return (geoBuffer, verts.count / 4)
}

func createCube(_ device : MTLDevice) -> (MTLBuffer?, MTLBuffer?, Int, Int)
{
  var verts : [CFloat] = [-0.5,  0.5, -0.5, 0.0, 0.0, -1.0,//0
    0.5,  0.5, -0.5, 0.0, 0.0, -1.0,//1
    0.5, -0.5, -0.5, 0.0, 0.0, -1.0,//2
    0.5, -0.5, -0.5, 0.0, 0.0, -1.0,//2
    -0.5, -0.5, -0.5, 0.0, 0.0, -1.0,//3
    -0.5,  0.5, -0.5, 0.0, 0.0, -1.0,//0
    
    0.5,  0.5, -0.5, 1.0,0.0,0.0, //1
    0.5,  0.5,  0.5, 1.0,0.0,0.0, //5
    0.5, -0.5,  0.5, 1.0,0.0,0.0, //6
    0.5, -0.5,  0.5, 1.0,0.0,0.0, //6
    0.5, -0.5, -0.5, 1.0,0.0,0.0, //2
    0.5,  0.5, -0.5, 1.0,0.0,0.0, //1
    
    0.5,  0.5,  0.5, 0.0,0.0,1.0, //5
    -0.5,  0.5,  0.5, 0.0,0.0,1.0, //4
    -0.5, -0.5,  0.5, 0.0,0.0,1.0, //7
    -0.5, -0.5,  0.5, 0.0,0.0,1.0, //7
    0.5, -0.5,  0.5, 0.0,0.0,1.0, //6
    0.5,  0.5,  0.5, 0.0,0.0,1.0, //5
    
    -0.5,  0.5,  0.5, -1.0,0.0,0.0, //4
    -0.5,  0.5, -0.5, -1.0,0.0,0.0, //0
    -0.5, -0.5, -0.5, -1.0,0.0,0.0, //3
    -0.5, -0.5, -0.5, -1.0,0.0,0.0, //3
    -0.5, -0.5,  0.5, -1.0,0.0,0.0, //7
    -0.5,  0.5,  0.5, -1.0,0.0,0.0, //4
    
    -0.5,  0.5,  0.5, 0.0,1.0,0.0,//4
    0.5,  0.5,  0.5, 0.0,1.0,0.0, //5
    0.5,  0.5, -0.5, 0.0,1.0,0.0, //1
    0.5,  0.5, -0.5, 0.0,1.0,0.0, //1
    -0.5,  0.5, -0.5, 0.0,1.0,0.0, //0
    -0.5,  0.5,  0.5, 0.0,1.0,0.0, //4
    
    -0.5, -0.5, -0.5, 0.0,-1.0,0.0, //3
    0.5, -0.5, -0.5, 0.0,-1.0,0.0, //2
    0.5, -0.5,  0.5, 0.0,-1.0,0.0, //6
    0.5, -0.5,  0.5, 0.0,-1.0,0.0, //6
    -0.5, -0.5,  0.5, 0.0,-1.0,0.0, //7
    -0.5, -0.5, -0.5, 0.0,-1.0,0.0, //3
  ]
  let geoBuffer = device.makeBuffer(length: verts.count*MemoryLayout<CFloat>.size, options: MTLResourceOptions.storageModeManaged)
  
  if let geoPtr = geoBuffer?.contents().assumingMemoryBound(to: CFloat.self) {
    geoPtr.assign(from: &verts, count: verts.count)
    geoBuffer!.didModifyRange(0..<verts.count*MemoryLayout<Float>.size)
  }
  return (geoBuffer, nil, 0, verts.count/6)
}

func getRotationAroundZ(_ radians : Float) -> matrix_float4x4
{
  var m : matrix_float4x4 = matrix_identity_float4x4;
  
  m.columns.0.x = cos(radians);
  m.columns.0.y = sin(radians);
  
  m.columns.1.x = -sin(radians);
  m.columns.1.y = cos(radians);
  
  return m;
}

func getRotationAroundY(_ radians : Float) -> matrix_float4x4
{
  var m : matrix_float4x4 = matrix_identity_float4x4;
  
  m.columns.0.x =  cos(radians);
  m.columns.0.z = -sin(radians);
  
  m.columns.2.x = sin(radians);
  m.columns.2.z = cos(radians);
  
  return m;
}

func getRotationAroundX(_ radians : Float) -> matrix_float4x4
{
  var m : matrix_float4x4 = matrix_identity_float4x4;
  
  m.columns.1.y = cos(radians);
  m.columns.1.z = sin(radians);
  
  m.columns.2.y = -sin(radians);
  m.columns.2.z =  cos(radians);
  
  return m;
}

func getTranslationMatrix(_ translation : vector_float4) -> matrix_float4x4
{
  var m : matrix_float4x4 = matrix_identity_float4x4
  
  m.columns.3 = translation
  
  return m
}

func getScaleMatrix(_ x : Float, y : Float, z : Float) -> matrix_float4x4
{
  var m = matrix_identity_float4x4
  
  m.columns.0.x = x
  m.columns.1.y = y
  m.columns.2.z = z
  
  return m
}

//Returns a value from -max to max
func getRandomValue(_ max : Double) -> Double
{
  let r : Int32 = Int32(Int64(arc4random()) - Int64(RAND_MAX))
  let v = (Double(r) / Double(RAND_MAX)) * max
  
  return v
}
