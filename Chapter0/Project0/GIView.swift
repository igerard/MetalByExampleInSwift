//
//  GIView.swift
//  Project0
//
//  Created by Gerard Iglesias on 05/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit

class GIView : NSView{
  override var bounds: NSRect{
    didSet{
      Swift.print(self.bounds.debugDescription)
    }
  }
  override var frame: NSRect{
    didSet{
      Swift.print("bounds : ", self.bounds.debugDescription)
      Swift.print("frame : ", self.frame.debugDescription)
    }
  }
  
  override func draw(_ dirtyRect: NSRect) {
    NSColor.blue.set()
    dirtyRect.fill()
  }
}
