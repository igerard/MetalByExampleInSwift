//
//  GIMTLView.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 01/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit

class GIMTLView : NSView {
  
  // MARK: Vars
  
  var info : String
  var mlLayer : CAMetalLayer? = nil
  override var layer: CALayer? {
    didSet {
      mlLayer = layer as? CAMetalLayer
    }
  }
  var device : MTLDevice
  var displayLink : CVDisplayLink? = nil
  
  override var frame: NSRect {
    didSet {
      self.mlLayer!.drawableSize = self.frame.size
    }
  }

  // MARK: Init Phase
  
  required init?(coder : NSCoder){
    if let theDevice = MTLCreateSystemDefaultDevice(){
      self.device = theDevice
      self.info = "View to show metal rendering"

      super.init(coder : coder)

      self.canDrawConcurrently = true
      self.wantsLayer = true
      self.layerContentsRedrawPolicy = .never
      self.layerContentsPlacement = .scaleAxesIndependently
    }
    else{
      return nil
    }
  }
  
  override func makeBackingLayer() -> CALayer {
    let mll = CAMetalLayer()
    mll.device = self.device
    mll.delegate = self
    Swift.print("Pixel format raw value : ", mll.pixelFormat.rawValue)
    Swift.print("Metal layer : ", mll)
    
    return mll
  }
  
  // MARK: Display
  
  fileprivate func renderFrame() {
    if let drawable = self.mlLayer?.nextDrawable() {
      //      Swift.print(mlLayer.description, " -- ", mlLayer.drawableSize.debugDescription)
      //      Swift.print(surface.description)
      
      let passDescr = MTLRenderPassDescriptor()
      passDescr.colorAttachments[0].texture = drawable.texture
      passDescr.colorAttachments[0].loadAction = .clear
      passDescr.colorAttachments[0].storeAction = .store
      
      let value = Double(sin(1.00 * CACurrentMediaTime()))
      passDescr.colorAttachments[0].clearColor = MTLClearColor(red: value, green: value, blue: value, alpha: 0.5)
      
      if let commandQueue = device.makeCommandQueue(),
        let commandBuffer = commandQueue.makeCommandBuffer(),
        let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){

        commandEncoder.endEncoding()
        drawable.present()
        commandBuffer.commit()
      }
    }
  }

  override func draw(_ dirtyRect: NSRect) {
    renderFrame()
  }
  
  override func viewDidMoveToWindow() {
    
    func GIDMLViewDisplayLinkRedrawCall(displayLink : CVDisplayLink,
                                        inNow : UnsafePointer<CVTimeStamp>,
                                        inOutputTime : UnsafePointer<CVTimeStamp>,
                                        flagsIn : CVOptionFlags,
                                        flagsOut : UnsafeMutablePointer<CVOptionFlags>,
                                        displayLinkContext : UnsafeMutableRawPointer?) -> CVReturn{
      _ = inNow[0]
      _ = inOutputTime[0]
      let view: GIMTLView = unsafeBitCast(displayLinkContext, to: GIMTLView.self)

      autoreleasepool{
        view.renderFrame()
      }

      return kCVReturnSuccess
    }

    if self.window != nil {
      CVDisplayLinkCreateWithCGDisplay(CGMainDisplayID(), &displayLink)
      if let link = displayLink {
        
        CVDisplayLinkSetOutputCallback(link, GIDMLViewDisplayLinkRedrawCall, UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()))
        
        CVDisplayLinkStart(link)
        
        Swift.print("Layer rect : ", mlLayer?.bounds.debugDescription ?? "No rect")
      }
    }
    else{
      if let link = displayLink {
        CVDisplayLinkStop(link)
        displayLink = nil
      }
    }
  }
  
  // MARK: Archive
  
  override func awakeFromNib() {
  }
  
  // MARK: Actions
  
  @IBAction func showInfo(_ sender : AnyObject) -> Void{
    Swift.print(self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
  }
  
  deinit {
    if let link = displayLink {
      CVDisplayLinkStop(link)
      displayLink = nil
    }
  }
  
}

extension GIMTLView : CALayerDelegate{
  
  @objc(displayLayer:) func display(_ layer: CALayer) {
    let drawable = self.mlLayer?.nextDrawable()
    
    Swift.print(self.layer?.description ?? "No layer")
    Swift.print(drawable?.description ?? "No rawable")
  }
  
  func layerWillDraw(_ layer: CALayer){
    Swift.print(self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
  }

}
