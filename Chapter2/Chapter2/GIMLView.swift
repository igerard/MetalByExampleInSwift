//
//  GIMTLView.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 01/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit

class GIMTLView : NSView {
  
  // MARK: Vars
  
  var info : String
  var mlLayer : CAMetalLayer? = nil
  override var layer: CALayer? {
    didSet {
      mlLayer = layer as? CAMetalLayer
    }
  }
  var device : MTLDevice
  var displayLink : CVDisplayLink? = nil
  
  // MARK: Init Phase
  
  required init?(coder : NSCoder){
    if let theDevice = MTLCreateSystemDefaultDevice(){
      self.device = theDevice
      self.info = "View to show metal rendering"
      
      super.init(coder : coder)
      
      self.wantsLayer = true
      self.layerContentsRedrawPolicy = .onSetNeedsDisplay
      self.layerContentsPlacement = .scaleAxesIndependently
    }
    else{
      return nil
    }
  }
  
  @objc override func makeBackingLayer() -> CALayer {
    let mll = CAMetalLayer()
    mll.device = self.device
    mll.delegate = self
    Swift.print("Pixel format raw value : ", mll.pixelFormat.rawValue)
    Swift.print("Metal layer : ", mll)
    
    return mll
  }
  
  // MARK: Display
  
  @objc override func draw(_ dirtyRect: NSRect) {
    redraw()
  }
  
  @objc override func updateLayer() {
    Swift.print(self.layer?.description ?? "No layer")
  }
  
  @objc override func viewDidMoveToWindow() -> Void {
    func GIDMLViewDisplayLinkRedrawCall(displayLink : CVDisplayLink,
                                        inNow : UnsafePointer<CVTimeStamp>,
                                        inOutputTime : UnsafePointer<CVTimeStamp>,
                                        flagsIn : CVOptionFlags,
                                        flagsOut : UnsafeMutablePointer<CVOptionFlags>,
                                        displayLinkContext : UnsafeMutableRawPointer?) -> CVReturn{
      _ = inNow[0]
      _ = inOutputTime[0]
      let view = unsafeBitCast(displayLinkContext, to: GIMTLView.self)
      
      autoreleasepool{
        view.redraw()
      }
      
      return kCVReturnSuccess
    }
    
    if self.window != nil {
      CVDisplayLinkCreateWithCGDisplay(CGMainDisplayID(), &displayLink)
      CVDisplayLinkSetOutputCallback(displayLink!, GIDMLViewDisplayLinkRedrawCall, UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()))
      
      CVDisplayLinkStart(displayLink!)
      
      Swift.print("Layer rect : ", mlLayer != nil ? mlLayer!.bounds.debugDescription : "Not defined")
    }
    else{
      if let link = displayLink {
        CVDisplayLinkStop(link)
        displayLink = nil
      }
    }
    redraw()
  }
  
  @objc override func resizeSubviews(withOldSize oldSize: NSSize) {
    super.resizeSubviews(withOldSize: oldSize)
    mlLayer?.drawableSize = CGSize(width: bounds.size.width, height: bounds.size.height)
  }
  
  func redraw() -> Void {
    if let drawable = self.mlLayer?.nextDrawable(){
      
      let passDescr = MTLRenderPassDescriptor()
      passDescr.colorAttachments[0].texture = drawable.texture
      passDescr.colorAttachments[0].loadAction = .clear
      passDescr.colorAttachments[0].storeAction = .store
      
      let value = Double(sin(1.00 * CACurrentMediaTime()))
      passDescr.colorAttachments[0].clearColor = MTLClearColor(red: value, green: value, blue: value, alpha: 0.5)
      
      if let commandQueue = device.makeCommandQueue(),
        let commandBuffer = commandQueue.makeCommandBuffer(),
        let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){
        
        commandEncoder.endEncoding()
        commandBuffer.present(drawable)
        commandBuffer.commit()
      }
    }
  }
  
  // MARK: Archive
  
  @objc override func awakeFromNib() {
    Swift.print(self.layer?.description ?? "No layer")
  }
  
  // MARK: Actions
  
  @IBAction func showInfo(_ sender : AnyObject) -> Void{
    Swift.print(self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
    needsDisplay = true
    redraw()
  }
  
}

// MARK: CALayerDelegate
extension GIMTLView : CALayerDelegate{
  
  @objc func display(_ layer: CALayer) {
    let drawable = self.mlLayer?.nextDrawable()
    
    Swift.print(self.layer?.description ?? "No layer")
    Swift.print(drawable?.description ?? "No rawable")
  }
  
  @objc func layerWillDraw(_ layer: CALayer){
    Swift.print(self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
  }
  
}
