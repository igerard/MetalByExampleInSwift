//
//  ViewController.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 01/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
  
  var mtlView : GIMTLView?

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    mtlView = self.view as? GIMTLView
  }

  override var representedObject: Any? {
    didSet {
    // Update the view, if already loaded.
    }
  }
  
}

