//
//  GIMTKView.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 03/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit
import Metal
import MetalKit

class GIMTKView : MTKView {
  
  // MARK: Vars
  
  var mlLayer : CAMetalLayer {
    get {
      return self.layer as! CAMetalLayer
    }
  }
  var info : String
  
  // MARK: Init Phase
  
  required init(coder : NSCoder){
    self.info = "View to show metal rendering"
    super.init(coder : coder)
    self.device = MTLCreateSystemDefaultDevice()
    self.preferredFramesPerSecond = 30
    self.autoResizeDrawable = true
  }
  
  // MARK: Display
  
  override func draw(_ rect : CGRect) {
    guard self.device != nil else{
      Swift.print("No device")
      return
    }
    
    if let drawable = self.currentDrawable{
      let value = Double(sin(1.00 * CACurrentMediaTime()))
      self.clearColor = MTLClearColor(red: value, green: value, blue: 1, alpha: 0.5)
      if let commandQueue = self.device?.makeCommandQueue(),
        let commandBuffer = commandQueue.makeCommandBuffer(),
        let passDescr = self.currentRenderPassDescriptor,
        let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){
        
        commandEncoder.endEncoding()
        commandBuffer.addScheduledHandler{_ in commandBuffer.present(drawable)}
        commandBuffer.commit()
      }
    }
  }
  
  // MARK: Archive
  
  override func awakeFromNib() {
    Swift.print(self.layer?.description ?? "No layer")
  }
  
  // MARK: Actions
  
  @IBAction func showInfo(_ sender : AnyObject) -> Void{
    Swift.print(self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
    Swift.print("Layer size : ", mlLayer.drawableSize.debugDescription)
  }
  
}
