//
//  AppDelegate.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 01/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

