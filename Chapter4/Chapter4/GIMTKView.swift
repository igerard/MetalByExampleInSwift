//
//  GIMTKView.swift
//  Chapter2
//
//  Created by Gerard Iglesias on 03/12/2016.
//  Copyright © 2016 Gerard Iglesias. All rights reserved.
//

import AppKit
import Metal
import MetalKit
import simd


/// The Vertex structure
struct SimpleVertex {
  var position : float4
  var color : float4
}

struct Uniforms
{
  var modelViewProjectionMatrix : matrix_float4x4
};

typealias GIMLIndex = UInt16
let GIMLIndexType : MTLIndexType = .uint16

class GIMTKView : MTKView {

  // MARK: Vars

  override var acceptsFirstResponder: Bool {
    get {
      return true
    }
  }

  override var canBecomeKeyView: Bool {
    get {
      return true
    }
  }

  var mlLayer : CAMetalLayer {
    get {
      return self.layer as! CAMetalLayer
    }
  }
  var info = "View to show metal rendering"

  // MARK Data Rendering

  var commandQueue : MTLCommandQueue?

  var vertexBuffer : MTLBuffer?
  var viewingBuffer : MTLBuffer?
  var indexBuffer : MTLBuffer?

  var library : MTLLibrary?
  var vertexFunction : MTLFunction?
  var fragmentFunction : MTLFunction?

  var pipelineState : MTLRenderPipelineState?
  var depthStencilState : MTLDepthStencilState?

  // MARK Viewing

  // an object to simulate a viewing camera and the transformations matrix.
  var camera = GIMTKCamera()

  // the struc to pass transformation to the shader
  var uniforms = Uniforms(modelViewProjectionMatrix: matrix_identity_float4x4)

  // MARK: Init Phase

  required init(coder : NSCoder){

    camera.position = float3(4,0,0)
    camera.direction = float3(-1,0,0)
    camera.up = float3(0,0,1)

    super.init(coder : coder)

    //autoResizeDrawable = true
    acceptsTouchEvents = true
    preferredFramesPerSecond = 60

    let value = 0.0
    clearColor = MTLClearColor(red: value, green: value, blue: value, alpha: 0.0)
    clearDepth = 1.0
    clearStencil = 0
    colorPixelFormat = .bgra8Unorm
    depthStencilPixelFormat = .depth32Float
    sampleCount = 4

    device = MTLCreateSystemDefaultDevice()
    if device != nil {
      commandQueue = self.device!.makeCommandQueue()
      makeBuffers()
      makePipeline()
      makeStencil()
    }
  }

  // make the needed buffer for viewing and drawing
  func makeBuffers(){
    // The goemetry vertex
    let valpha = Float(0.333)
    let vertices : [SimpleVertex] = [
      SimpleVertex(position: float4(-1,  1,  1, 1), color: float4(0, 1, 1, valpha)),
      SimpleVertex(position: float4(-1, -1,  1, 1), color: float4(0, 0, 1, valpha)),
      SimpleVertex(position: float4( 1, -1,  1, 1), color: float4(1, 0, 1, valpha)),
      SimpleVertex(position: float4( 1,  1,  1, 1), color: float4(1, 1, 1, valpha)),
      SimpleVertex(position: float4(-1,  1, -1, 1), color: float4(0, 1, 0, valpha)),
      SimpleVertex(position: float4(-1, -1, -1, 1), color: float4(0, 0, 0, valpha)),
      SimpleVertex(position: float4( 1, -1, -1, 1), color: float4(1, 0, 0, valpha)),
      SimpleVertex(position: float4( 1,  1, -1, 1), color: float4(1, 1, 0, valpha)),
      ]
    vertexBuffer = self.device!.makeBuffer(bytes: vertices,
                                           length: vertices.count*MemoryLayout<SimpleVertex>.size,
                                           options: .storageModeShared)
    // the geometry indexes
    let indexes : [GIMLIndex] = [
      3,2,6,
      6,7,3,
      4,5,1,
      1,0,4,
      4,0,3,
      3,7,4,
      1,5,6,
      6,2,1,
      0,1,2,
      2,3,0,
      7,6,5,
      5,4,7
    ]
    indexBuffer = self.device!.makeBuffer(bytes: indexes,
                                          length: indexes.count * MemoryLayout<GIMLIndex>.size,
                                          options: .cpuCacheModeWriteCombined)
    // the viewing buffer
    let viewingBufferSize = MemoryLayout<Uniforms>.size
    viewingBuffer = self.device?.makeBuffer(length: viewingBufferSize, options: .cpuCacheModeWriteCombined)
  }

  // refresh the viewing transformation
  // rotate the model arounf Z axis, at a given period of revolution
  func update3DViewBuffer(){
    // compute time angle relative to the current time
    let period : Int = 10
    let currentTime : TimeInterval = Date.timeIntervalSinceReferenceDate
    let currentTimeInSlice = currentTime - Double(Int(currentTime) - Int(currentTime) % period)
    let timeAngle = 2 * .pi * currentTimeInSlice / Double(period)

    // perpective
    let perspective = getPerpectiveProjectionMatrix(.pi / 3.0, aspectRatio: Float(bounds.size.width)/Float(bounds.size.height), zFar: 10.0, zNear: 1.0)
    // viewing
    let viewing = camera.GetViewMatrix()
    // model
    let model = getRotationAroundZ(Float(timeAngle))

    // compute the matrix composition
    uniforms.modelViewProjectionMatrix = matrix_multiply(perspective, matrix_multiply(viewing, model))

    memcpy(viewingBuffer?.contents(), &uniforms, MemoryLayout<Uniforms>.size)
  }

  // Build the pipeline...
  func makePipeline() {
    if let library = self.device!.makeDefaultLibrary() {
      if let vertexFunction = library.makeFunction(name : "vertex_project"),
        let fragmentFunction = library.makeFunction(name : "fragment_flatcolor") {

        let pipelineDescr = MTLRenderPipelineDescriptor()
        pipelineDescr.vertexFunction = vertexFunction
        pipelineDescr.fragmentFunction = fragmentFunction
        pipelineDescr.colorAttachments[0].pixelFormat = mlLayer.pixelFormat

        pipelineDescr.colorAttachments[0].isBlendingEnabled = true

        pipelineDescr.colorAttachments[0].alphaBlendOperation = .add
        pipelineDescr.colorAttachments[0].sourceAlphaBlendFactor = .sourceAlpha
        pipelineDescr.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusSourceAlpha

        pipelineDescr.colorAttachments[0].rgbBlendOperation = .add
        pipelineDescr.colorAttachments[0].sourceRGBBlendFactor = .sourceAlpha
        pipelineDescr.colorAttachments[0].destinationRGBBlendFactor = .oneMinusSourceAlpha

        pipelineDescr.depthAttachmentPixelFormat = .depth32Float
        pipelineDescr.sampleCount = 4

        do {
          pipelineState = try device!.makeRenderPipelineState(descriptor: pipelineDescr)
        }
        catch {
          Swift.print(error)
        }
      }
    }
  }
  
  // Build the pipeline...
  func makeStencil(){
    let depthStencilDescriptor = MTLDepthStencilDescriptor()
    depthStencilDescriptor.isDepthWriteEnabled = true
    depthStencilDescriptor.depthCompareFunction = .less

    depthStencilState = device!.makeDepthStencilState(descriptor: depthStencilDescriptor)
  }
  
  // MARK: Display

  override func draw(_ rect : CGRect) {
    guard self.device != nil else{
      Swift.print("No device")
      return
    }
    guard self.commandQueue != nil else{
      Swift.print("No Command Queue")
      return
    }
    guard self.pipelineState != nil else{
      Swift.print("No pipeline")
      return
    }

    update3DViewBuffer()

    if let drawable = self.currentDrawable,
      let commandBuffer = commandQueue!.makeCommandBuffer(),
      let passDescr = self.currentRenderPassDescriptor,
      let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){
        commandEncoder.setRenderPipelineState(pipelineState!)
        commandEncoder.setDepthStencilState(depthStencilState)

        commandEncoder.setFrontFacing(.counterClockwise)

        // set the vertex of the geometry
        commandEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        // send the constants to the GPU
        commandEncoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)

        // draw back faces
        commandEncoder.setCullMode(.front)
        commandEncoder.drawIndexedPrimitives(type: .triangle, indexCount: 36, indexType: GIMLIndexType, indexBuffer: indexBuffer!, indexBufferOffset: 0)

        // draw front faces
        commandEncoder.setCullMode(.back)
        commandEncoder.drawIndexedPrimitives(type: .triangle, indexCount: 36, indexType: GIMLIndexType, indexBuffer: indexBuffer!, indexBufferOffset: 0)

        commandEncoder.endEncoding()

        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
  }

  // MARK: Events

  override func becomeFirstResponder() -> Bool {
    return true
  }

  // MARK: Layouts

  // MARK: Archive

  override func awakeFromNib() {
    Swift.print(self.layer?.description ?? "No layer")
  }

  // MARK: Actions

  @IBAction func showInfo(_ sender : AnyObject) -> Void{
    Swift.print("INFO : ", self.description , " - ", self.layer?.description ?? "No layer")
    Swift.print("   layer delegate : ", self.layer?.delegate?.description ?? "No layer delegate")
    Swift.print("   Layer size : ", mlLayer.drawableSize.debugDescription)
  }
  
}
